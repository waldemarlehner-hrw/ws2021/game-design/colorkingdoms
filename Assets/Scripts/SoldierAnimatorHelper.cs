using Helpers;
using UnityEngine;

public class SoldierAnimatorHelper : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private void Start()
    {
        this.animator = this.gameObject.GetComponent<Animator>();
    }

    public void NotifyAboutWalking()
    {
        this.animator.Play("Soldier_Walking");
    }

    public void NotifyAboutTurnMoved()
    {
        this.animator.Play("Soldier_Turn_Moved");
    }

    public void NotifyAboutReady()
    {
        this.animator.Play("Soldier_Ready");
    }

    public void NotifyAboutInfluence()
    {
        this.animator.Play("Soldier_Influence");
    }

    public void NotifyAboutCutOff()
    {
        this.animator.Play("Soldier_Lost");
    }

    public void HandleNewTurnStarted(SoldierEnums.Position positionalState)
    {
        if (positionalState == SoldierEnums.Position.CutOff)
        {
            this.NotifyAboutCutOff();
        }
        else
        {
            this.NotifyAboutReady();
        }
    }
}
