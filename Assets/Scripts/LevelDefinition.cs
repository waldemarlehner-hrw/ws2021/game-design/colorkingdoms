using System;
using System.Collections.Generic;
using System.Linq;
using Helpers;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class LevelDefinition
{
    public enum TileType
    {
        None,
        Regular,
        Kingdom,
        House
    }

    private readonly Dictionary<(int x, int y), TileType> tiles;
    private readonly (int x, int y) size;

    public LevelDefinition(string text)
    {
        var relevantLines = text.Split('\n').Where(e => e.Trim().StartsWith("#") == false).ToArray();
        this.size = LevelDefinitionHelper.ParseMapSize(relevantLines[0]);
        this.tiles = LevelDefinitionHelper.ParseMap(relevantLines);
    }

    public void Apply(BoardManager boardManager, ICollection<Kingdom> kingdoms)
    {
        LevelDefinitionHelper.ReduceKingdomTilesToPlayerCount(this.tiles, kingdoms.Count);
        // Randomize Order in which Kingdoms appear
        var kingdomsRandomOrder = new Queue<Kingdom>(kingdoms.OrderBy(e => Random.value).ToList());
        var kingdomTiles = new List<TileState>();

        boardManager.Reset();

        var generatedTiles = new TileState[this.size.x, this.size.y];
        for (var x = 0; x < this.size.x; x++)
        {
            for (var y = 0; y < this.size.y; y++)
            {
                if (this.tiles.ContainsKey((x,y)) == false || this.tiles[(x,y)] == TileType.None)
                {
                    continue;
                }

                generatedTiles[x,y] = this.HandleIndividualTile(boardManager, x, y, kingdomsRandomOrder);
                if (this.tiles[(x, y)] == TileType.Kingdom)
                {
                    kingdomTiles.Add(generatedTiles[x,y]);
                }


            }
        }


        boardManager.PushNewTiles(generatedTiles, kingdomTiles);
    }


    private TileState HandleIndividualTile(BoardManager boardManager, int x, int y, Queue<Kingdom> kingdoms)
    {
        var tileType = this.tiles[(x,y)];

        var instanceToUse = GetCorrectPrefab(tileType, boardManager);
        var tileInstance = Object.Instantiate(instanceToUse, boardManager.transform, true);
        tileInstance.name = GetTileName(x,y,tileType);
        var tileState = tileInstance.GetComponent<TileState>();

        if (tileType == TileType.Kingdom)
        {
            tileState.Init(x, y, tileType, boardManager.GetRandomTileTexture(), kingdoms.Dequeue());
        }
        else
        {
            tileState.Init(x, y, tileType, boardManager.GetRandomTileTexture());
        }


        tileInstance.transform.position = LevelDefinitionHelper.CalculateTilePosition(boardManager, x, y);
        return tileState;
    }

    private static string GetTileName(int xPos, int yPos, TileType tileType) =>
        $"Tile-{xPos}-{yPos}-{Enum.GetName(typeof(TileType), tileType)}";


    private static GameObject GetCorrectPrefab(TileType tileType, BoardManager boardManager)
    {
        return tileType switch
        {
            TileType.Kingdom => boardManager.kingdomTileInstance,
            TileType.House => boardManager.houseTileInstance,
            TileType.Regular => boardManager.tileInstance,
            TileType.None => throw new ArgumentException("Unexpected Tile Type"),
            _ => throw new ArgumentOutOfRangeException(nameof(tileType), tileType, null)
        };
    }
}