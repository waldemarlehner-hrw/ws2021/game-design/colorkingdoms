using System.Collections.Generic;
using System.Linq;

namespace Helpers.Extensions
{
    public static class TileStateExtensions
    {
        public static IReadOnlyList<(Kingdom kingdom, int count)> GetInfluencersSorted(this TileState tile)
        {
            var tilesRelevantForInfluenceCalculation = tile.GetSurroundingTiles();
            tilesRelevantForInfluenceCalculation.Add(tile);
            var kingdomCounter = new Dictionary<Kingdom, int>();
            foreach (var entry in tilesRelevantForInfluenceCalculation)
            {
                if (entry.Soldier == null)
                {
                    continue;
                }

                var soldierKingdom = entry.Soldier.Kingdom;

                if (!kingdomCounter.ContainsKey(soldierKingdom))
                {
                    kingdomCounter[soldierKingdom] = 0;
                }

                kingdomCounter[soldierKingdom]++;
            }

            var sortedByKingdoms =
                from pair in kingdomCounter
                orderby pair.Value descending
                select (kingdom: pair.Key, count: pair.Value);
            return sortedByKingdoms.ToList();
        }

        public static Kingdom GetInfluenceWinner(this TileState tile)
        {
            var allKingdomsSortedByInfluence = tile.GetInfluencersSorted();
            if (allKingdomsSortedByInfluence.Count == 0)
            {
                return null;
            }

            if (allKingdomsSortedByInfluence.Count == 1)
            {
                return allKingdomsSortedByInfluence[0].kingdom;
            }

            var first = allKingdomsSortedByInfluence[0];
            var second = allKingdomsSortedByInfluence[1];

            if (first.count == second.count)
            {
                return null;
            }

            return first.kingdom;
        }

        public static IReadOnlyList<TileState> GetTilesCurrentOccupierCanMoveTo(this TileState tileState)
        {
            if (tileState.Soldier == null)
            {
                return new List<TileState>();
            }

            var surroundingTiles = tileState.GetSurroundingTiles();
            return surroundingTiles.Where(e => tileState.Soldier.CanMoveToNewTile(e)).ToList();
        }
    }
}