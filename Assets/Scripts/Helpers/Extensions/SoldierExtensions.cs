using System.Linq;

namespace Helpers.Extensions
{
    public static class SoldierExtensions
    {
        /**
         * Check if this Soldier can move to the provided Tile.
         */
        public static bool CanMoveToNewTile(this Soldier soldier, TileState tile)
        {
            if (tile.Type == LevelDefinition.TileType.House)
            {
                return false;
            }

            if (tile.Kingdom == soldier.Kingdom && !tile.IsCutOff)
            {
                //hasTileConnectionToBase = true;
            }
            else if (tile.GetSurroundingTiles().Any(e => e.Kingdom == soldier.Kingdom && !e.IsCutOff))
            {
                //hasTileConnectionToBase = true;
            }
            else
            {
                return false;
            }

            if (tile.Soldier != null)
            {
                // Cannot attack friendly units
                if (tile.Soldier.Kingdom == soldier.Kingdom)
                {
                    return false;
                }

                // To be able to attack you need to have one adjacent friendly unit. Note that the friendly unit does not
                // need to have remaining move points. It just needs to be adjacent to the enemy when attacking.
                // The attacker needs to be adjacent to the enemy when attacking. Its movement point is used up during the
                // attack. The attacker does not move towards the enemies field when defeating the enemy.
                var canBeAttacked = tile.GetSurroundingTiles()
                    .Count( t => SoldierHelper.CanSoldiersOnThisTileAssistInAttack(soldier, t)) >= 2;
                return canBeAttacked;
            }

            return true;
        }

        public static bool CanMoveThisTurn(this Soldier soldier)
        {
            var currentPositionState = soldier.PositionalState;
            var didSoldierAlreadyMove = soldier.State != SoldierEnums.State.NeedsToMove;
            if (didSoldierAlreadyMove)
            {
                return false;
            }

            var hasSoldierConnectionToBase = (currentPositionState == SoldierEnums.Position.FriendlyField ||
                                              currentPositionState == SoldierEnums.Position.AdjacentToFriendlyField);
            return hasSoldierConnectionToBase;
        }
    }
}