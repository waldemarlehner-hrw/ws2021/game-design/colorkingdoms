using System;
using System.Collections.Generic;
using System.Linq;

namespace Helpers.Clustering
{
    /**
     * This Class is used to determine which Tiles are connected with each other
     */
    public class Cluster
    {

        private TileState Identity => this.Root.TileState;

        private Cluster Root
        {
            get
            {
                var c = this.parent;
                while (c != c.parent)
                {
                    c = c.parent;
                }

                return c;
            }
        }

        private Cluster parent;
        public TileState TileState { get; }
        private readonly LinkedList<Cluster> children = new LinkedList<Cluster>();

        public IReadOnlyCollection<Cluster> Children => this.children;

        private Cluster(TileState tile)
        {
            this.parent = this;
            this.TileState = tile;
        }

        public static Dictionary<Kingdom, IList<ClusterResult>> BuildClusters(IReadOnlyList<TileState> tiles)
        {
            // Create initial 1-Entry large Clusters
            Dictionary<TileState, Cluster> clusterLookup = new Dictionary<TileState, Cluster>();
            foreach (var tile in tiles)
            {
                clusterLookup[tile] = new Cluster(tile);
            }
            // Now, merge clusters for each Tile until the Size of the Cluster Lookup no longer changes
            RunMergeIteration(tiles, clusterLookup);


            var clusterRoots = GetClusterRoots(clusterLookup.Values);

            var allClustersWithKingdom = clusterRoots.Where(c => c.Identity.Kingdom != null);

            // Make sure only Roots are in the list
            var clusterResults = allClustersWithKingdom.Select(e => new ClusterResult(e.Root));

            var returnDict = new Dictionary<Kingdom, IList<ClusterResult>>();
            foreach (var clusterResult in clusterResults)
            {
                if (!returnDict.ContainsKey(clusterResult.Kingdom))
                {
                    returnDict[clusterResult.Kingdom] = new List<ClusterResult>();
                }

                returnDict[clusterResult.Kingdom].Add(clusterResult);
            }

            return returnDict;

        }


        private static HashSet<Cluster> GetClusterRoots(IEnumerable<Cluster> clusters)
        {
            HashSet<Cluster> roots = new HashSet<Cluster>();
            foreach (var cluster in clusters)
            {
                roots.Add(cluster.Root);
            }

            return roots;
        }


        private static void RunMergeIteration(IEnumerable<TileState> tiles, IReadOnlyDictionary<TileState, Cluster> clusterLookup)
        {
            foreach (var tile in tiles)
            {
                if (((object)tile.Kingdom) == null)
                {
                    continue;
                }

                var clusterCandidates = tile.GetSurroundingTiles().Where(e => e.Kingdom == tile.Kingdom);
                MergeClusters(tile, clusterCandidates, clusterLookup);
            }
        }

        private static void MergeClusters(TileState tile, IEnumerable<TileState> clusterCandidates, IReadOnlyDictionary<TileState, Cluster> clusterLookup)
        {
            foreach (var candidate in clusterCandidates)
            {
                var candidateCluster = clusterLookup[candidate];
                var ownCluster = clusterLookup[tile];
                if (candidateCluster.Root != ownCluster.Root)
                {
                    ownCluster.MergeWith(candidateCluster);
                }
            }
        }

        private void MergeWith(Cluster child)
        {
            var childRoot = child.Root;
            childRoot.parent = this;
            this.children.AddLast(childRoot);
        }

        public override string ToString()
        {
            return this.TileState.name;
        }
    }
}