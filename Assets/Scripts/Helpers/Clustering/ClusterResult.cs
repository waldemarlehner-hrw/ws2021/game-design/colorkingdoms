using System.Collections.Generic;
using System.Linq;

namespace Helpers.Clustering
{
    public class ClusterResult
    {
        public Kingdom Kingdom { get; }
        private readonly HashSet<TileState> tileLookUp = new HashSet<TileState>();

        public ClusterResult(Cluster root)
        {
            this.Kingdom = root.TileState.Kingdom;
            foreach (var child in GetChildrenRecursive(root, new LinkedList<TileState>()))
            {
                this.tileLookUp.Add(child);
            }
        }

        public IEnumerable<TileState> GetAll()
        {
            return this.tileLookUp.ToList();
        }

        public bool Contains(TileState tile)
        {
            return this.tileLookUp.Contains(tile);
        }

        private static IEnumerable<TileState> GetChildrenRecursive(Cluster cluster, LinkedList<TileState> listToAppendTo)
        {
            foreach (var child in cluster.Children)
            {
                GetChildrenRecursive(child, listToAppendTo);
            }

            listToAppendTo.AddFirst(cluster.TileState);
            return listToAppendTo;
        }

    }
}