using System.Collections.Generic;

namespace Helpers.Clustering
{
    public class CutOffTilesResult
    {
        private readonly Dictionary<Kingdom, List<TileState>> activeTileLookUp = new Dictionary<Kingdom, List<TileState>>();
        private readonly Dictionary<Kingdom, List<TileState>> inactiveTileLookUp = new Dictionary<Kingdom, List<TileState>>();

        public CutOffTilesResult(IReadOnlyList<TileState> allTiles)
        {
            var kingdomTiles = new Dictionary<Kingdom, TileState>();
            foreach (var entry in allTiles)
            {
                if (entry.IsKingdom && entry.Kingdom != null)
                {
                    kingdomTiles[entry.Kingdom] = entry;
                }
            }

            var clusterResult = Cluster.BuildClusters(allTiles);
            foreach (var kingdomAndClusters in clusterResult)
            {
                if (!kingdomTiles.ContainsKey(kingdomAndClusters.Key))
                {
                    // This may happen when the Kingdom Tile has changed colors
                    continue;
                }
                var kingdomTile = kingdomTiles[kingdomAndClusters.Key];
                var clusters = kingdomAndClusters.Value;
                foreach (var cluster in clusters)
                {
                    var dictionaryToAddTo =
                        cluster.Contains(kingdomTile) ? this.activeTileLookUp : this.inactiveTileLookUp;

                    if (!dictionaryToAddTo.ContainsKey(kingdomAndClusters.Key))
                    {
                        dictionaryToAddTo[kingdomAndClusters.Key] = new List<TileState>();
                    }

                    dictionaryToAddTo[kingdomAndClusters.Key].AddRange(cluster.GetAll());
                }
            }
        }

        public (List<TileState> active, List<TileState> inactive) GetForKingdom(Kingdom kingdom)
        {
            var active = this.activeTileLookUp[kingdom] ?? new List<TileState>();
            var inactive = this.inactiveTileLookUp[kingdom] ?? new List<TileState>();

            return (active, inactive);
        }

        public void NotifyAllTilesStatesAboutNewCutOffState()
        {
            foreach (var activeTileList in this.activeTileLookUp.Values)
            {
                foreach (var entry in activeTileList)
                {
                    entry.NotifyAboutNewCutOffState(false);
                }
            }

            foreach (var inactivateTileList in this.inactiveTileLookUp.Values)
            {
                foreach (var entry in inactivateTileList)
                {
                    entry.NotifyAboutNewCutOffState(true);
                }
            }

        }
    }
}