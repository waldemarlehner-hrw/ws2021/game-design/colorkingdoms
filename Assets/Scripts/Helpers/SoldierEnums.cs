namespace Helpers
{
    public static class SoldierEnums
    {
        public enum Position
        {
            /// <summary>
            /// The Soldier is on a Tile that is friendly (has same Kingdom) which is directly connected to the Kingdom Base
            /// </summary>
            FriendlyField,

            /// <summary>
            /// The Soldier is next to a friendly (connected) field. As a result, it can accept orders.
            /// But it cannot attack any enemies from this position.
            /// </summary>
            AdjacentToFriendlyField,

            /// <summary>
            /// The Soldier is not connected to its Kingdom, and as a result, cannot accept commands.
            /// </summary>
            CutOff
        }

        public enum State
        {
            /// <summary>
            /// The Soldier has yet to move this turn.
            /// </summary>
            NeedsToMove,

            /// <summary>
            /// The Soldier was moved this turn already.
            /// </summary>
            MovedTile,

            /// <summary>
            /// The Soldier attacked an enemy, and as a result can no longer move this turn
            /// </summary>
            KilledEnemy,

            /// <summary>
            /// The Soldier stayed at the current position, spreading influence to adjacent tiles in the process
            /// </summary>
            SpreadInfluence,

            /// <summary>
            /// This is used when this Soldier is not being moved. As in, when another Kingdom is currently on their turn.
            /// </summary>
            Static,
        }
    }
}