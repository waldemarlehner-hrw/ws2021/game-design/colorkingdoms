using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Helpers
{
    internal static class LevelDefinitionHelper
    {
        private static LevelDefinition.TileType ParseTextDefinitionOfTile(char c)
        {
            return c switch
            {
                '-' => LevelDefinition.TileType.None,
                'x' => LevelDefinition.TileType.Regular,
                'o' => LevelDefinition.TileType.Kingdom,
                'h' => LevelDefinition.TileType.House,
                _ => throw new LevelParseException($"Received an unexpected character: {c}")
            };
        }

        internal static (int x, int y) ParseMapSize(string dimensionsString)
        {
            var xAndyCount = dimensionsString.Split(':');
            var x = int.Parse(xAndyCount[0]);
            var y = int.Parse(xAndyCount[1]);
            return (x, y);
        }

        internal static Dictionary<(int x, int y), LevelDefinition.TileType> ParseMap(IReadOnlyList<string> relevantLines)
        {
            var dimensions = ParseMapSize(relevantLines[0]);
            if (dimensions.y != relevantLines.Count - 1)
            {
                throw new LevelParseException($"Expected {dimensions.y} Rows, but found {relevantLines.Count - 1}");
            }

            var lookup = new Dictionary<(int x, int y), LevelDefinition.TileType>();

            for(var y = 1; y < relevantLines.Count; y++)
            {
                var row = relevantLines[y];
                var rowWithoutSpaceChars = row.Where(e => !char.IsWhiteSpace(e)).ToArray();
                if (rowWithoutSpaceChars.Length != dimensions.x)
                {
                    throw new LevelParseException($"Expected a Row Length of {dimensions.x}, but only found {rowWithoutSpaceChars.Length}. This occured in Row {y}");
                }

                for(var x = 0; x < rowWithoutSpaceChars.Length; x++)
                {
                    var value = rowWithoutSpaceChars[x];
                    lookup[(x, y - 1)] = ParseTextDefinitionOfTile(value);
                }
            }

            return lookup;
        }


        internal static Vector3 CalculateTilePosition(BoardManager boardManager, int x, int y)
        {
            var position = new Vector3(x * boardManager.xOffset, -y * boardManager.yOffset);
            if (y % 2 != 0)
            {
                position += Vector3.right * boardManager.xOffset / 2f;
            }

            return position;
        }

        internal static void ReduceKingdomTilesToPlayerCount(Dictionary<(int x, int y), LevelDefinition.TileType> tiles,
            int playerCount)
        {
            var kingdomIndices = new List<(int x, int y)>();

            foreach (var entry in tiles)
            {
                if (entry.Value == LevelDefinition.TileType.Kingdom)
                {
                    kingdomIndices.Add(entry.Key);
                }
            }

            // Randomize List
            kingdomIndices = kingdomIndices.OrderBy(e => Random.value).ToList();
            // Indices to turn into Regular tiles
            // if players are A, and total kingdom tiles are B, then amount of tiles to turn into Regular is
            // = B - A
            var kingdomTileIndicesToTurnIntoRegularTiles = kingdomIndices.Take(kingdomIndices.Count - playerCount);
            foreach (var (x,y) in kingdomTileIndicesToTurnIntoRegularTiles)
            {
                tiles[(x, y)] = LevelDefinition.TileType.Regular;
            }
        }

        private class LevelParseException : Exception
        {
            internal LevelParseException(string message) : base(message)
            {
            }
        }

    }


}