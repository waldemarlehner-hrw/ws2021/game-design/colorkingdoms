using System.Linq;
using UnityEngine;

namespace Helpers
{
    internal static class SoldierHelper
    {
        internal static bool CanSoldiersOnThisTileAssistInAttack(Soldier soldier, TileState tile)
        {
            // No Soldier on this tile
            if (tile.Soldier == null)
            {
                return false;
            }

            // Soldier is an enemy
            if (tile.Soldier.Kingdom != soldier.Kingdom)
            {
                return false;
            }

            // Soldier is friendly, but does not have influence over its tile
            if (tile.Kingdom != soldier.Kingdom)
            {
                return false;
            }

            // Soldier is cut-off
            if (tile.Soldier.PositionalState == SoldierEnums.Position.CutOff)
            {
                return false;
            }

            return true;
        }

        internal static Color CalculateCurrentColor(Soldier soldier)
        {
            var showTurnColors = soldier.State != SoldierEnums.State.Static;
            var currentPosition = soldier.PositionalState;
            if (showTurnColors)
            {
                if (currentPosition == SoldierEnums.Position.FriendlyField ||
                    currentPosition == SoldierEnums.Position.AdjacentToFriendlyField)
                {
                    // Can be moved: Show green
                    // Was already moved: Show original color
                    return soldier.State == SoldierEnums.State.NeedsToMove ? Color.green : soldier.Kingdom.Color;

                }
                else
                {
                    // Soldier is deactivated
                    return Color.Lerp(soldier.Kingdom.Color, Color.black, 0.8f);
                }
            }
            else
            {
                // Simply display the Soldier "Regular" Color
                return soldier.Kingdom.Color;
            }
        }

        internal static SoldierEnums.Position CalculatePositionalState(Soldier soldier)
        {
            if (soldier.CurrentTile.GetSurroundingTiles().Any(entry => entry.Kingdom == soldier.Kingdom && !entry.IsCutOff))
            {
                return SoldierEnums.Position.AdjacentToFriendlyField;
            }

            if (soldier.CurrentTile.IsCutOff)
            {
                return SoldierEnums.Position.CutOff;
            }

            if (soldier.Kingdom == soldier.CurrentTile.Kingdom)
            {
                return SoldierEnums.Position.FriendlyField;
            }

            return SoldierEnums.Position.CutOff;
        }


    }
}