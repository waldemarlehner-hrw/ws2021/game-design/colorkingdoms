using System.Collections.Generic;
using System.Linq;
using Helpers.Extensions;
using UnityEngine;

namespace Helpers
{
    internal static class TileStateHelper
    {
        internal static Color GetColorBasedOnState(TileState state)
        {
            Color color;
            if (state.Kingdom == null)
            {
                color = Color.clear;
            }
            else if (state.IsCutOff)
            {
                color = Color.Lerp(state.Kingdom.Color, Color.black, 0.5f);
            }
            else
            {
                color = state.Kingdom.Color;
            }
            return color;
        }

        internal static ICollection<TileState> GetTilesToApplyInfluenceTo(TileState tile)
        {
            var surroundingTiles = tile.GetSurroundingTiles();
            var surroundingWhereCurrentOccupantWinsInfluence =
                (from e in surroundingTiles
                    where e.GetInfluenceWinner() == tile.Soldier.Kingdom
                    select e).ToList();

            if (tile.GetInfluenceWinner() == tile.Soldier.Kingdom)
            {
                surroundingWhereCurrentOccupantWinsInfluence.Add(tile);
            }

            return surroundingWhereCurrentOccupantWinsInfluence;
        }
    }
}