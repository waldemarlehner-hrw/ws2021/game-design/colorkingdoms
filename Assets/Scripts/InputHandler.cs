using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField]
    private Camera main;



    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            GameManager.Instance.NotifyAboutMouseUp(this.GetTileTargetedByMouse(), true);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            GameManager.Instance.NotifyAboutMouseUp(this.GetTileTargetedByMouse(), false);
        }
        else if (Input.GetMouseButtonDown(0))
        {
            GameManager.Instance.NotifyAboutMouseDown(this.GetTileTargetedByMouse(), true);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            GameManager.Instance.NotifyAboutMouseDown(this.GetTileTargetedByMouse(), false);
        }
    }


    TileState GetTileTargetedByMouse()
    {
        var mousePosition = main.ScreenPointToRay(Input.mousePosition);
        var rays = Physics2D.GetRayIntersectionAll(mousePosition);
        for (int i = 0; i < rays.Length; i++)
        {
            bool hasComponent = rays[i].collider.gameObject.TryGetComponent<TileState>(out var tile);
            if (!hasComponent)
            {
                continue;
            }

            return tile;
        }

        return null;
    }
}
