using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalManager : MonoBehaviour
{
    public static GlobalManager Instance { get; private set; }
    public Kingdom[] playerKingdoms;
    public Color[] kingdomColors;
    public Sprite[] kingdomSprites;
    public int playerCount;
    public int level;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        this.playerCount = 2;

        GameObject kingdomRoot = new GameObject("Kingdoms");
        this.playerKingdoms = new Kingdom[3];

        for (int i = 0; i < 3; i++)
        {
            var kingdomGameObject = new GameObject($"Kingdom {i + 1}");
            this.playerKingdoms[i] = kingdomGameObject.AddComponent<Kingdom>();
            this.playerKingdoms[i].KingdomCastleSprite = this.kingdomSprites[i];
            kingdomGameObject.transform.parent = kingdomRoot.transform;
        }
    }

    public static void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }

    public static void LoadGame()
    {
        SceneManager.LoadScene(1);
    }
}