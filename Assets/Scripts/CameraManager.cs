using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    #region DEBUG Data
    [SerializeField, ReadOnlyInspector] private Vector3 dragStartPositionScreenSpace;
    [SerializeField, ReadOnlyInspector] private Vector3 currentMouseOverPositionScreenSpace;
    [SerializeField, ReadOnlyInspector] private Vector3 targetPosition;
    [SerializeField, ReadOnlyInspector] private bool isDragging = false;
    [SerializeField, ReadOnlyInspector] private float targetZoom;
    #endregion

    [SerializeField, Range(0,1)] private float positionLerp;
    [SerializeField] private float deadZonePadding = 20f;
    [SerializeField] private BoardManager boardManager;
    [SerializeField] private float speedMultiplier;
    [SerializeField] private new Camera camera;
    [SerializeField] private float maxZoom;
    [SerializeField] private float minZoom;
    [SerializeField] private float zoomStep;
    [SerializeField, Range(0, 1)] private float zoomLerp;

    private Vector3 startPositionOfCamera;

    private void Start()
    {
        this.targetZoom = this.camera.orthographicSize;
        this.targetPosition = this.camera.transform.position;
        this.boardManager.NotifyBuildCompleteEvent += this.HandleBoardManagerBuildCompleteEvent;
    }

    private void HandleBoardManagerBuildCompleteEvent()
    {
        var bounds = this.boardManager.Bounds;
        var cameraTransform = this.camera.transform;
        var newCameraPosition = new Vector3(
            (bounds.minX + bounds.maxX) / 2,
            (bounds.minY + bounds.maxY) / 2,
            cameraTransform.position.z
        );

        cameraTransform.position = newCameraPosition;
        this.targetPosition = newCameraPosition;
    }

    // Update is called once per frame
    void Update()
    {


        this.currentMouseOverPositionScreenSpace = Input.mousePosition;
        if (Input.GetMouseButtonDown(2))
        {
            this.startPositionOfCamera = this.camera.transform.position;
            this.dragStartPositionScreenSpace = this.currentMouseOverPositionScreenSpace;
            this.isDragging = true;
        }

        if (Input.GetMouseButtonUp(2))
        {
            this.isDragging = false;
        }

        if (this.isDragging)
        {
            var deltaVectorInScreenSpace = this.currentMouseOverPositionScreenSpace - this.dragStartPositionScreenSpace;
            var deltaMultipliedWithOrthoDistance = deltaVectorInScreenSpace * this.speedMultiplier * this.camera.orthographicSize;
            this.targetPosition = this.startPositionOfCamera - deltaMultipliedWithOrthoDistance;
            var bounds = this.boardManager.Bounds;
            var x = Mathf.Clamp(this.targetPosition.x, bounds.minX - this.deadZonePadding,
                bounds.maxX + this.deadZonePadding);
            var y = Mathf.Clamp(this.targetPosition.y, bounds.minY - this.deadZonePadding,
                bounds.maxY + this.deadZonePadding);

            this.targetPosition = new Vector3(x, y, this.targetPosition.z);

        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            this.targetZoom -= Input.GetAxis("Mouse ScrollWheel") * this.zoomStep;
            this.targetZoom = Mathf.Clamp(this.targetZoom, this.minZoom, this.maxZoom);
        }
        this.camera.transform.position =
            Vector3.Lerp(this.camera.transform.position, this.targetPosition, this.positionLerp);
        this.camera.orthographicSize =
            Mathf.Lerp(this.camera.orthographicSize, this.targetZoom, this.zoomLerp);
    }
}
