using System;
using UnityEngine;

public class HouseTile : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRendererOfHouseObject;

    private TileState tileState;
    private bool isInit;
    private Action conversionCallback;

    public void Init(TileState tileState, Action conversionCallback)
    {
        if (this.isInit)
        {
            throw new Exception("Already initialized");
        }
        this.tileState = tileState;
        this.isInit = true;
        this.tileState.KingdomChangedFromToEvent += this.HandleKingdomChangedFromToEvent;
        this.conversionCallback = conversionCallback;
    }

    private void HandleKingdomChangedFromToEvent(Kingdom from, Kingdom to)
    {
        this.SpawnSoldier(to);
        this.ConvertToRegularTile();
        this.tileState.KingdomChangedFromToEvent -= this.HandleKingdomChangedFromToEvent;
    }

    private void SpawnSoldier(Kingdom allegiance)
    {
        allegiance.SpawnSoldierOnTile(this.tileState);
        GameManager.Instance.playAudio("house");
    }

    private void ConvertToRegularTile()
    {
        // Remove House Sprite
        if (this.spriteRendererOfHouseObject != null)
        {
            Destroy(this.spriteRendererOfHouseObject.gameObject);
        }
        this.conversionCallback.Invoke();
        Destroy(this);
    }
}
