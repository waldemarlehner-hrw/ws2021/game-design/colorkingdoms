using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudio : MonoBehaviour
{

    [SerializeField] private AudioSource AudioMusic;
    [SerializeField] private AudioSource AudioSounds;

    [SerializeField] private AudioClip StartSound;
    [SerializeField] private AudioClip Vicotry;
    [SerializeField] private AudioClip TurnEnd;
    [SerializeField] private AudioClip Fight;
    [SerializeField] private AudioClip SoldierMove;
    [SerializeField] private AudioClip SoldierSit;
    [SerializeField] private AudioClip House;

    void Start()
    {
        GameManager.Instance.TurnStartEvent += this.PlayTurnStart;
        GameManager.Instance.WinnerChosenEvent += this.PlayWin;
        this.playAudio(this.StartSound);
    }

    void Update()
    {
    }

    public void PlayTurnStart(Kingdom kingdomNowPlaying)
    {
        this.playAudio(TurnEnd);
    }

    public void PlayFight()
    {
        this.playAudio(Fight, 0.4f);
    }

    public void PlayMove()
    {
        this.playAudio(SoldierMove);
    }

    public void PlaySit()
    {
        this.playAudio(SoldierSit);
    }

    public void PlayHouse()
    {
        this.playAudio(House);
    }
    
    public void PlayWin(Kingdom kingdomNowPlaying)
    {
        this.AudioMusic.Stop();
        this.playAudio(Vicotry, 0.3f);
    }

    private void playAudio(AudioClip clip)
    {
        this.AudioSounds.PlayOneShot(clip);
    }
    private void playAudio(AudioClip clip, float vol)
    {
        this.AudioSounds.PlayOneShot(clip, vol);
    }
}
