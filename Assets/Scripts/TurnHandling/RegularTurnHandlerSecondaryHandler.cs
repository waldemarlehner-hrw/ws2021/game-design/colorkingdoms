using System.Collections.Generic;
using System.Linq;
using Helpers.Extensions;
using UnityEngine;

namespace TurnHandling
{
    internal class RegularTurnHandlerSecondaryHandler
    {
        private readonly RegularTurnHandlerData data;
        private readonly ICollection<TileState> tilesWithTemporaryColors = new List<TileState>();
        public RegularTurnHandlerSecondaryHandler(RegularTurnHandlerData data)
        {
            this.data = data;
        }

        public void NotifyAboutMouseDown(TileState tileOrNull)
        {
            if (tileOrNull == null || !this.data.TilesWhereActionCanBeInitiated.Contains(tileOrNull))
            {
                this.data.SelectedTile = null;
                return;
            }

            this.data.SelectedTile = tileOrNull;
            var tilesThatWouldGetAffectedByInfluenceSpread = this.CalculateInfluenceSpread(tileOrNull);
            foreach (var entry in tilesThatWouldGetAffectedByInfluenceSpread)
            {
                this.tilesWithTemporaryColors.Add(entry);
                var currentColor = entry.GetColor();
                entry.SetColor(Color.Lerp(currentColor, this.data.Kingdom.Color, 0.5f));
            }

            if (tileOrNull.GetInfluenceWinner() == this.data.Kingdom)
            {
                // Soldier is influence winner on own tile
                tileOrNull.SetColor(Color.white);
            }
            else
            {
                tileOrNull.SetColor(Color.Lerp(tileOrNull.Kingdom != null ? tileOrNull.Kingdom.Color : Color.gray, Color.black, 0.3f ));
            }
        }

        private IEnumerable<TileState> CalculateInfluenceSpread(TileState tileState)
        {
            var surroundingTiles = tileState.GetSurroundingTiles();
            var surroundingTilesWithInfluenceWinner =
                from tile in surroundingTiles
                select (tile, influence: tile.GetInfluenceWinner());
            var influenceWinnerOnOwnTile = tileState.GetInfluenceWinner();

            var tilesWhereCurrentKingdomIsWinner =
                from tile in surroundingTilesWithInfluenceWinner
                where tile.influence == this.data.Kingdom
                select tile.tile;

            var asList = tilesWhereCurrentKingdomIsWinner.ToList();
            if (influenceWinnerOnOwnTile == tileState.Soldier.Kingdom)
            {
                // TODO: Could set that spreading influence will always turn own field.
                // To do this, set the if statement to true
                asList.Add(tileState);
            }

            return asList;
        }

        public void NotifyAboutMouseUp(TileState tileOrNull)
        {
            var didApply = false;

            if (tileOrNull != null && tileOrNull == this.data.SelectedTile)
            {
                var soldier = tileOrNull.Soldier;
                soldier.ApplyInfluence();
                this.data.SoldiersThatMoved.Add(soldier);
                this.data.SoldiersToMove.Remove(soldier);

                didApply = true;
            }

            this.ResetModifiedColors();

            if (this.data.SelectedTile != null)
            {
                this.data.SelectedTile.ResetColor();
            }

            if (didApply)
            {
                this.data.TilesWhereActionCanBeInitiated.Remove(tileOrNull);
                this.data.NotifyAboutPostMove();
            }

            this.data.SelectedTile = null;
        }

        private void ResetModifiedColors()
        {
            foreach (var modifiedTile in this.tilesWithTemporaryColors)
            {
                modifiedTile.ResetColor();
            }
            this.tilesWithTemporaryColors.Clear();
        }
    }
}