using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TurnHandling
{
    public class SoldierStartPositionSelectionTurnHandler : ITurnHandler
    {
        private static readonly Color StartOptionColor = Color.cyan;
        private static readonly Color SelectionMouseDownColor = Color.white;

        private int soldiersToSpawn;
        private readonly Kingdom kingdom;
        private TileState selectedTile = null;

        private readonly HashSet<TileState> firstPositionCandidates = new HashSet<TileState>();

        public event Action TurnFinishedEvent;

        public SoldierStartPositionSelectionTurnHandler(int soldierCount, Kingdom kingdom, BoardManager boardManager)
        {
            this.soldiersToSpawn = soldierCount;
            this.kingdom = kingdom;

            var kingdomTile = boardManager.GetKingdomTile(kingdom);
            var surrounding = boardManager.GetTilesSurrounding(kingdomTile.Position);
            var unoccupiedTiles =
                surrounding.Where(e => e.Type == LevelDefinition.TileType.Regular && e.Soldier == null);
            foreach (var entry in unoccupiedTiles)
            {
                this.firstPositionCandidates.Add(entry);
            }
            foreach (var entry in this.firstPositionCandidates)
            {
                entry.SetColor(StartOptionColor);
            }
        }

        private void NotifyAboutMouseDown(TileState tileOrNull)
        {
            if (tileOrNull == null || !firstPositionCandidates.Contains(tileOrNull))
            {
                this.selectedTile = null;
                return;
            }

            this.selectedTile = tileOrNull;
            this.selectedTile.SetColor(SelectionMouseDownColor);
        }

        private void NotifyAboutMouseUp(TileState tileOrNull)
        {
            if (tileOrNull == this.selectedTile && tileOrNull != null)
            {
                this.PlaceNewSoldierOnTile(tileOrNull);
            }
            else if(this.selectedTile != null)
            {
                this.selectedTile.SetColor(StartOptionColor);
                this.selectedTile = null;
            }
        }

        public void NotifyAboutMouseDown(TileState tileOrNull, bool isPrimary)
        {
            //. No secondary actions
            if (isPrimary)
            {
                this.NotifyAboutMouseDown(tileOrNull);
            }
        }

        public void NotifyAboutMouseUp(TileState tileOrNull, bool isPrimary)
        {
            // No secondary actions
            if (isPrimary)
            {
                this.NotifyAboutMouseUp(tileOrNull);
            }
        }

        private void PlaceNewSoldierOnTile(TileState tile)
        {
            this.kingdom.SpawnSoldierOnTile(tile);
            tile.ResetColor();
            this.firstPositionCandidates.Remove(tile);
            this.soldiersToSpawn--;
            if (this.soldiersToSpawn == 0)
            {
                foreach (var entry in this.firstPositionCandidates)
                {
                    entry.ResetColor();
                }
                this.firstPositionCandidates.Clear();
                this.TurnFinishedEvent?.Invoke();
            }
        }
    }
}