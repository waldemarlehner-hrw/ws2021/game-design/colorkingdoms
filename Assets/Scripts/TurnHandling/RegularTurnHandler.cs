using System;

namespace TurnHandling
{
    public class RegularTurnHandler : ITurnHandler
    {
        private enum TurnAction
        {
            None,
            Primary,
            Secondary
        }

        private TurnAction currentAction = TurnAction.None;
        private readonly RegularTurnHandlerData data;

        private readonly RegularTurnHandlerPrimaryHandler primaryHandler;
        private readonly RegularTurnHandlerSecondaryHandler secondaryHandler;

        public int MovableSoldierCount => this.data.TilesWhereActionCanBeInitiated.Count;

        public event Action TurnFinishedEvent;

        public RegularTurnHandler(Kingdom kingdom, BoardManager boardManager)
        {
            _ = (object) kingdom != null ? kingdom : throw new NullReferenceException();
            kingdom.NotifyAboutNewTurn();
            this.data = new RegularTurnHandlerData(kingdom, boardManager);
            this.data.TurnFinishedEvent += () => this.TurnFinishedEvent?.Invoke();

            this.primaryHandler = new RegularTurnHandlerPrimaryHandler(this.data);
            this.secondaryHandler = new RegularTurnHandlerSecondaryHandler(this.data);
        }



        public void NotifyAboutMouseDown(TileState tileOrNull, bool isPrimary)
        {
            if (this.currentAction != TurnAction.None)
            {
                return;
            }

            if (isPrimary)
            {
                this.currentAction = TurnAction.Primary;
                this.primaryHandler.NotifyAboutMouseDown(tileOrNull);
            }
            else
            {
                this.currentAction = TurnAction.Secondary;
                this.secondaryHandler.NotifyAboutMouseDown(tileOrNull);
            }
        }


        public void NotifyAboutMouseUp(TileState tileOrNull, bool isPrimary)
        {
            if (this.currentAction == TurnAction.Primary && isPrimary)
            {
                this.primaryHandler.NotifyAboutMouseUp(tileOrNull);
                this.currentAction = TurnAction.None;
            }
            else if (this.currentAction == TurnAction.Secondary && !isPrimary)
            {
                this.secondaryHandler.NotifyAboutMouseUp(tileOrNull);
                this.currentAction = TurnAction.None;
            }
        }



        public void SetSoldierMoved(Soldier soldier)
        {
            this.data.NotifySoldierMoved(soldier);
            this.data.NotifyAboutPostMove();
        }
    }
}