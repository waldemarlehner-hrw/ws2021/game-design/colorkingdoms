using System;
using System.Collections.Generic;

namespace TurnHandling
{
    public class RingBuffer<T>
    {
        private readonly LinkedList<T> list;

        public event Action<T> OnlyOneEntryRemainingEvent;

        public RingBuffer(IEnumerable<T> data)
        {
            this.list = new LinkedList<T>(data);
        }

        public T Peek()
        {
            return this.list.First.Value;
        }

        public void MoveHeadToTail()
        {
            var head = this.Peek();
            this.list.RemoveFirst();
            this.list.AddLast(head);
        }

        public int Count => this.list.Count;

        public T Pop()
        {
            var entry = this.list.First.Value;
            this.list.RemoveFirst();
            if (this.Count == 1)
            {
                this.OnlyOneEntryRemainingEvent?.Invoke(this.Peek());
            }
            return entry;
        }

        public void Remove(T value)
        {
            this.list.Remove(value);
            if (this.Count == 1)
            {
                this.OnlyOneEntryRemainingEvent?.Invoke(this.Peek());
            }
        }
    }
}