using System;

namespace TurnHandling
{
    public interface ITurnHandler
    {
        void NotifyAboutMouseDown(TileState tileOrNull, bool isPrimary);

        void NotifyAboutMouseUp(TileState tileOrNull, bool isPrimary);

        event Action TurnFinishedEvent;
    }
}