using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Helpers;
using Helpers.Extensions;

namespace TurnHandling
{
    /// <summary>
    /// Data-Class. Is passed to Primary and Secondary Turn-Handlers.
    /// </summary>
    internal class RegularTurnHandlerData
    {
        public RegularTurnHandlerData(Kingdom kingdom, BoardManager boardManager)
        {
            this.Kingdom = kingdom;
            this.BoardManager = boardManager;

            foreach (var soldier in kingdom.Soldiers)
            {
                this.SoldiersToMove.Add(soldier);
                if (soldier.CanMoveThisTurn())
                {
                    this.TilesWhereActionCanBeInitiated.Add(soldier.CurrentTile);
                }
            }
        }

        private BoardManager BoardManager { get; }

        public Kingdom Kingdom { get; }
        public TileState SelectedTile { get; set; } = null;
        public ICollection<TileState> TilesWhereActionCanBeInitiated { get; } = new Collection<TileState>();

        public ICollection<Soldier> SoldiersToMove { get; } = new Collection<Soldier>();

        public ICollection<Soldier> SoldiersThatMoved { get; } = new Collection<Soldier>();

        public event Action TurnFinishedEvent;

        public void NotifySoldierMoved(Soldier soldier)
        {
            this.SoldiersToMove.Remove(soldier);
            this.SoldiersThatMoved.Add(soldier);
            this.TilesWhereActionCanBeInitiated.Remove(soldier.CurrentTile);
        }

        public void NotifyAboutPostMove()
        {
            this.TilesWhereActionCanBeInitiated.Clear();
            this.BoardManager.RunCutOffCalculations();
            this.RebuildTilesWhereActionCanBeInitiatedState();

            if (!this.SoldiersToMove.Any(e => e.CanMoveThisTurn()))
            {
                this.Kingdom.NotifyAboutTurnFinished();
                this.TurnFinishedEvent?.Invoke();
            }
        }

        private void RebuildTilesWhereActionCanBeInitiatedState()
        {

            foreach (var entry in this.SoldiersToMove)
            {
                var hasSoldierCompletedTurn = entry.State != SoldierEnums.State.NeedsToMove;
                var hasSoldierConnectionToBase = entry.PositionalState == SoldierEnums.Position.FriendlyField ||
                                                 entry.PositionalState ==
                                                 SoldierEnums.Position.AdjacentToFriendlyField;

                if (!hasSoldierCompletedTurn && hasSoldierConnectionToBase )
                {
                    this.TilesWhereActionCanBeInitiated.Add(entry.CurrentTile);
                }
            }
        }
    }
}