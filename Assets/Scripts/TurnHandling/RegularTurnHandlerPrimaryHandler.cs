using System.Collections.Generic;
using Helpers.Extensions;
using UnityEngine;

namespace TurnHandling
{
    public class RegularTurnHandlerPrimaryHandler
    {
        private readonly RegularTurnHandlerData data;
        private readonly ICollection<TileState> tilesWithTemporaryColors = new List<TileState>();
        private readonly ICollection<TileState> tilesWhereMouseCanBeReleased = new List<TileState>();

        internal RegularTurnHandlerPrimaryHandler(RegularTurnHandlerData data)
        {
            this.data = data;
        }


        public void NotifyAboutMouseDown(TileState tileOrNull)
        {
            if (tileOrNull == null || !this.data.TilesWhereActionCanBeInitiated.Contains(tileOrNull))
            {
                this.data.SelectedTile = null;
                return;
            }

            this.data.SelectedTile = tileOrNull;
            
            // Get Tiles the Soldier on the Tile can move to
            foreach (var tile in tileOrNull.GetTilesCurrentOccupierCanMoveTo())
            {
                var color = tile.Kingdom != null ? tile.Kingdom.Color : Color.gray;
                if (tile.Soldier == null)
                {
                    // No attack
                    color = Color.Lerp(color, Color.white, 0.5f);
                }
                else
                {
                    // Attack taking place
                    color = Color.Lerp(Color.red, color, 0.3f);
                }
                
                tile.SetColor(color);
                this.tilesWithTemporaryColors.Add(tile);
                this.tilesWhereMouseCanBeReleased.Add(tile);
            }
        }

        public void NotifyAboutMouseUp(TileState tileOrNull)
        {
            this.ResetModifiedColors();
            var didActionTakePlace = false;

            if (tileOrNull == null)
            {
                this.data.SelectedTile = null;
            }
            else if (this.tilesWhereMouseCanBeReleased.Contains(tileOrNull))
            {
                if (tileOrNull.Soldier == null)
                {
                    this.MoveSoldierToNewTile(this.data.SelectedTile.Soldier, tileOrNull);
                }
                else
                {
                    this.KillEnemySoldier(this.data.SelectedTile.Soldier, tileOrNull.Soldier);
                }
                // TODO: Is this a bug? Should this be this.data.SelectedTile instead?
                this.data.TilesWhereActionCanBeInitiated.Remove(tileOrNull);

                didActionTakePlace = true;
            }
            else
            {
                this.data.SelectedTile = null;
            }

            this.tilesWhereMouseCanBeReleased.Clear();
            this.ResetModifiedColors();

            if (didActionTakePlace)
            {
                this.data.NotifyAboutPostMove();
            }
        }

        private void KillEnemySoldier(Soldier attacker, Soldier victim)
        {
            attacker.Attack(victim);
            this.data.SoldiersThatMoved.Add(attacker);
            this.data.SoldiersToMove.Remove(attacker);
        }

        private void MoveSoldierToNewTile(Soldier soldierToMove, TileState soldierTarget)
        {
            soldierToMove.MoveToNewTile(soldierTarget);
            this.data.SoldiersThatMoved.Add(soldierToMove);
            this.data.SoldiersToMove.Remove(soldierToMove);
        }

        private void ResetModifiedColors()
        {
            foreach (var modifiedTile in this.tilesWithTemporaryColors)
            {
                modifiedTile.ResetColor();
            }

            this.tilesWithTemporaryColors.Clear();
        }


    }
}