using System;
using Helpers;
using Helpers.Extensions;
using UnityEngine;

public class Soldier : MonoBehaviour
{
    private const float PositionLinearLerpValue = 4f;
    [SerializeField, ReadOnlyInspector] private Kingdom kingdom;
    [SerializeField, ReadOnlyInspector] private SoldierEnums.State soldierState;

    public Kingdom Kingdom => this.kingdom;

    [SerializeField, ReadOnlyInspector] private TileState currentTile;
    [SerializeField, ReadOnlyInspector] private Material soldierMaterial;

    public TileState CurrentTile
    {
        get => this.currentTile;
        private set
        {
            if (this.currentTile != null)
            {
                this.currentTile.CutOffEvent -= this.HandleCutoffEvent;
            }

            this.currentTile = value;
            this.currentTile.CutOffEvent += this.HandleCutoffEvent;
        }
    }

    public event Action OnDeadEvent;

    public SoldierEnums.Position PositionalState => SoldierHelper.CalculatePositionalState(this);

    public SoldierEnums.State State => this.soldierState;


    private void Awake()
    {
        this.soldierMaterial = this.gameObject.transform.Find("Accent").GetComponent<Renderer>().material;
        if (this.soldierMaterial == null)
        {
            throw new NullReferenceException("Failed to get Renderer");
        }
    }

    public bool isMoving = false;

    private void Update()
    {
        if (!this.isMoving)
        {
            return;
        }


        if (this.transform.localPosition == Vector3.zero)
        {
            this.isMoving = false;
            this.animator.NotifyAboutTurnMoved();
            return;
        }

        var oldPosition = this.transform.localPosition;
        var lerpValue = PositionLinearLerpValue * Time.deltaTime;
        var newPosition = oldPosition - oldPosition.normalized * lerpValue;

        if (Vector3.Dot(oldPosition, newPosition) < 0)
        {
            // Vectors are opposite: Overshot target
            // See https://www.geogebra.org/m/Yu6869By
            newPosition = Vector3.zero;
            this.animator.NotifyAboutTurnMoved();
            this.isMoving = false;
        }

        this.transform.localPosition = newPosition;
    }

    private bool isInit = false;

    [SerializeField]
    private SoldierAnimatorHelper animator;


    public void Init(Kingdom kingdom, TileState tile)
    {
        if (this.isInit)
        {
            throw new Exception();
        }

        kingdom.NewTurnStartedEvent += this.HandleNewTurnStarted;
        kingdom.TurnFinishedEvent += this.HandleTurnComplete;
        kingdom.SubscribeDefeatInternal(this.HandleKingdomDefeat);
        this.kingdom = kingdom;
        this.soldierState = SoldierEnums.State.Static;
        this.CurrentTile = tile;
        this.transform.position = tile.transform.position;
        this.isInit = true;
    }

    private void HandleNewTurnStarted()
    {
        this.soldierState = SoldierEnums.State.NeedsToMove;
        this.animator.HandleNewTurnStarted(this.PositionalState);
        this.UpdateColor();
    }

    private void HandleTurnComplete()
    {
        this.soldierState = SoldierEnums.State.Static;
        this.UpdateColor();
    }

    public void MoveToNewTile(TileState newTile)
    {
        if (this.CanMoveToNewTile(newTile))
        {
            newTile.MoveSoldierHere(this);
            this.CurrentTile = newTile;
            this.soldierState = SoldierEnums.State.MovedTile;
            this.animator.NotifyAboutWalking();
            this.isMoving = true;
            this.UpdateColor();
            GameManager.Instance.playAudio("move");
        }
        else
        {
            throw new Exception("Cannot move at this time");
        }
    }

    private void SetColor(Color color)
    {
        this.soldierMaterial.color = color;
    }

    private void HandleCutoffEvent(bool isCutOff)
    {
        if (this.kingdom != this.CurrentTile.Kingdom)
        {
            // Received an Event that the tile was cut off.
            // Note that this Event should only affect "friendly" Units.
            // E.g. when Blue is Cut Off, but a Yellow Solider is on the field, the Yellow Solider should not be
            // disabled.
            Debug.Log("Soldier on a foreign field that changed cut-off state: " + isCutOff);
            return;
        }

        Debug.Log("Soldier cut-off state changed to " + isCutOff);
        this.animator.NotifyAboutCutOff();
        this.UpdateColor();
    }

    public void UpdateColor()
    {
        this.SetColor(SoldierHelper.CalculateCurrentColor(this));
    }


    public void ApplyInfluence()
    {
        this.soldierState = SoldierEnums.State.SpreadInfluence;
        this.currentTile.ApplyInfluence();
        this.animator.NotifyAboutInfluence();
        this.UpdateColor();
        GameManager.Instance.playAudio("sit");
    }

    public void Attack(Soldier attackTarget)
    {
        attackTarget.Die();
        this.soldierState = SoldierEnums.State.KilledEnemy;
        this.UpdateColor();
        GameManager.Instance.playAudio("fight");
    }

    private void Die()
    {
        this.Kingdom.NewTurnStartedEvent -= this.HandleNewTurnStarted;
        this.Kingdom.TurnFinishedEvent -= this.HandleTurnComplete;
        this.Kingdom.UnsubscribeDefeatInternal(this.HandleKingdomDefeat);
        this.currentTile.NotifyAboutOccupantDying();
        this.currentTile = null;
        this.OnDeadEvent?.Invoke();
        Destroy(this.gameObject);
    }

    /// <summary>
    /// This Method should only be invoked by the Kingdom.
    /// </summary>
    private void HandleKingdomDefeat()
    {
        this.Die();
    }
}