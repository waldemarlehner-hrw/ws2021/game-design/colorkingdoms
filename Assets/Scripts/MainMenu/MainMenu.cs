using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour
{
    private int[] selColorsOld = new int[3];

    [SerializeField] private GameObject BtnPlay;
    [SerializeField] private GameObject BtnAdd;
    [SerializeField] private GameObject BtnRem;
    [SerializeField] private GameObject[] BtnSize;
    [SerializeField] private GameObject[] DropKingdom;
    [SerializeField] private GameObject SliVolValue;
    [SerializeField] private GameObject TxtVolValue;
    [SerializeField] private Dropdown DropRes;


    [SerializeField] private GameObject[] RulesText;
    [SerializeField] private GameObject RulesIndexTxt;

    [SerializeField] private AudioSource AudioMusic;
    [SerializeField] private AudioMixer AudioMixer;

    [SerializeField] private AudioClip Loop;

    private Resolution[] resolutions;

    private int RulesIndex = 0;


    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            DropKingdom[i].GetComponent<Dropdown>().value = i;
            selColorsOld[i] = i;
        }


        resolutions = Screen.resolutions;
        DropRes.ClearOptions();
        List<string> options = new List<string>();

        float f;
        this.AudioMixer.GetFloat("volume", out f);
        SliVolValue.GetComponent<Slider>().value = f;
        float vol = (1 - ((f * -1) / 40)) * 100;
        TxtVolValue.GetComponent<Text>().text = vol.ToString();

        this.SetRules();

        int resIndex = 0;
        for (int i = 0; i <resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                resIndex = i;
            }
        }

        DropRes.AddOptions(options);
        DropRes.value = resIndex;
        DropRes.RefreshShownValue();
    }

    void Update()
    {
        if (GlobalManager.Instance.level != 0)
        {
            BtnPlay.gameObject.GetComponent<Button>().interactable = true;
            BtnPlay.GetComponentInChildren<Text>().color = Color.black;
        }
        else
        {
            BtnPlay.gameObject.GetComponent<Button>().interactable = false;
            BtnPlay.GetComponentInChildren<Text>().color = Color.red;
        }

        if (!this.AudioMusic.isPlaying)
        {
            this.AudioMusic.clip = Loop;
            this.AudioMusic.Play();
        }
    }

    public void PlayGame()
    {
        string tmp = "";
        for (int i = 0; i < 3; i++)
        {
            tmp += 'I';
            GlobalManager.Instance.playerKingdoms[i].Color = GlobalManager.Instance.kingdomColors[DropKingdom[i].GetComponent<Dropdown>().value];
            GlobalManager.Instance.playerKingdoms[i].Name = "Player " + tmp;
        }

        if(GlobalManager.Instance.playerCount < 3)
        {
            GlobalManager.Instance.playerKingdoms[2] = null;
        }

        GlobalManager.LoadGame();
        Destroy(this.gameObject);
    }

    public void SetVolume(float vol)
    {
        AudioMixer.SetFloat("volume", vol);
        vol = (1 - ((vol * -1) / 40)) * 100;
        TxtVolValue.GetComponent<Text>().text = vol.ToString();
    }

    public void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resIndex)
    {
        Resolution resolution = resolutions[resIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void AddKingdom()
    {
        GlobalManager.Instance.playerCount++;
        if(GlobalManager.Instance.playerCount == 3)
        {
            BtnAdd.GetComponent<Button>().interactable = false;
            BtnAdd.GetComponentInChildren<Text>().color = Color.red;
            BtnRem.GetComponent<Button>().interactable = true;
            BtnRem.GetComponentInChildren<Text>().color = Color.white;
        }
    }

    public void RemoveKingdom()
    {
        GlobalManager.Instance.playerCount--;
        if (GlobalManager.Instance.playerCount == 2)
        {
            BtnAdd.GetComponent<Button>().interactable = true;
            BtnAdd.GetComponentInChildren<Text>().color = Color.white;
            BtnRem.GetComponent<Button>().interactable = false;
            BtnRem.GetComponentInChildren<Text>().color = Color.red;
        }
    }
    public void ColorSelect(int i)
    {
        try
        {
            int val = DropKingdom[i].GetComponent<Dropdown>().value;
            DropKingdom[selColorsOld[val]].GetComponent<Dropdown>().value = selColorsOld[i];

            for (int j = 0; j < 3; j++)
            {
                selColorsOld[j] = DropKingdom[j].GetComponent<Dropdown>().value;
            }
        }
        catch
        {
            for (int j = 0; j < 3; j++)
            {
                selColorsOld[j] = j;
                DropKingdom[j].GetComponent<Dropdown>().value = j;
            }
        }
    }

    public void ButtonSelected(int lvl)
    {
        GlobalManager.Instance.level = lvl;
        for (int i = 0; i < BtnSize.Length; i++)
        {
            BtnSize[i].GetComponentInChildren<Text>().color = Color.black;
        }

        try { 
            BtnSize[lvl - 1].GetComponentInChildren<Text>().color = Color.white;
        }
        catch { }
    }

    public void RulesNext()
    {
        if(RulesIndex != RulesText.Length-1)
        {
            this.RulesIndex++;
        }
        else
        {
            this.RulesIndex = 0;
        }
        this.SetRules();
    }

    public void RulesBack()
    {
        if (RulesIndex != 0)
        {
            this.RulesIndex--;
        }
        else
        {
            this.RulesIndex = RulesText.Length-1;
        }
        this.SetRules();
    }

    private void SetRules()
    {
        for(int i = 0; i<RulesText.Length; i++)
        {
            RulesText[i].SetActive(false);
        }

        RulesText[RulesIndex].SetActive(true);
        
        RulesIndexTxt.GetComponent<Text>().text = RulesIndex+1 + "/" + RulesText.Length;
    }

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
