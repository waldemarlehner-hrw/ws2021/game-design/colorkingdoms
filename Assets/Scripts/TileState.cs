using System;
using System.Collections.Generic;
using Helpers;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileState : MonoBehaviour
{
    [SerializeField, ReadOnlyInspector] private Kingdom kingdom = null;

    [SerializeField, ReadOnlyInspector] private Soldier soldier = null;

    [SerializeField, ReadOnlyInspector] private bool isKingdom = false;

    [SerializeField, ReadOnlyInspector] private Material tileMaterial;

    [SerializeField, ReadOnlyInspector] private bool isCutOff = false;

    [SerializeField, ReadOnlyInspector] private bool isHouseTile;

    public Soldier Soldier => this.soldier;

    public Kingdom Kingdom
    {
        get => this.kingdom;
        private set
        {
            if (this.kingdom == value)
            {
                return;
            }

            var oldValue = this.kingdom;
            this.kingdom = value;

            this.KingdomChangedFromToEvent?.Invoke(oldValue, value);
        }
    }

    public bool IsKingdom => this.isKingdom;

    public (int x, int y) Position => this.position;
    public LevelDefinition.TileType Type { get; private set; }
    public bool IsCutOff => this.isCutOff;

    private BoardManager boardManager;

    public event Action<Kingdom, Kingdom> KingdomChangedFromToEvent;

    private void Awake()
    {
        this.tileMaterial = this.gameObject.transform.Find("hexFBX").GetComponent<Renderer>().material;
    }

    private void Start()
    {
        this.boardManager = this.gameObject.GetComponentInParent<BoardManager>();
    }

    private (int x, int y) position;
    private bool init = false;


    public void Init(int x, int y, LevelDefinition.TileType tileType, Sprite texture, Kingdom initialKingdom = null)
    {
        if (this.init)
        {
            return;
        }

        this.Kingdom = initialKingdom;
        this.init = true;
        this.position = (x, y);
        this.Type = tileType;

        var backgroundSpriteRenderer = this.transform.Find("Background").GetComponent<SpriteRenderer>();
        backgroundSpriteRenderer.sprite = texture;
        if (Random.value > 0.5f)
        {
            backgroundSpriteRenderer.flipX = true;
        }

        if (tileType == LevelDefinition.TileType.Kingdom)
        {
            this.isKingdom = true;
            this.gameObject.GetComponent<KingdomTile>().Init(this.kingdom);
        }

        if (tileType == LevelDefinition.TileType.House)
        {
            this.isHouseTile = true;
            this.gameObject.GetComponent<HouseTile>().Init(this, this.HandleConversionToRegularTile);
        }
        this.ResetColor();
    }

    private void HandleConversionToRegularTile()
    {
        this.Type = LevelDefinition.TileType.Regular;
    }

    public List<TileState> GetSurroundingTiles() => this.boardManager.GetTilesSurrounding(this.position);


    public void MoveSoldierHere(Soldier soldier)
    {
        if (soldier.CurrentTile != null)
        {
            soldier.CurrentTile.soldier = null;
        }

        this.soldier = soldier;
        if (this.kingdom == null)
        {
            this.Kingdom = soldier.Kingdom;
            this.ResetColor();
        }

        var soldierTransform = soldier.gameObject.transform;
        soldierTransform.parent = this.gameObject.transform;

        //soldierTransform.localPosition = Vector3.zero;
    }

    public void SetColor(Color color)
    {
        this.tileMaterial.color = color;
    }

    public void ResetColor() => this.SetColor(TileStateHelper.GetColorBasedOnState(this));

    public Color GetColor() => this.tileMaterial.color;

    public void ApplyInfluence()
    {
        if (this.soldier == null)
        {
            return;
        }

        foreach (var tileState in TileStateHelper.GetTilesToApplyInfluenceTo(this))
        {
            tileState.Kingdom = this.soldier.Kingdom;
            tileState.ResetColor();
        }
    }

    public void NotifyAboutNewCutOffState(bool newCutOffState)
    {
        if (newCutOffState == !this.isCutOff)
        {
            this.isCutOff = newCutOffState;
            this.CutOffEvent?.Invoke(newCutOffState);
            this.ResetColor();
        }
    }

    public event Action<bool> CutOffEvent;

    public void NotifyAboutOccupantDying()
    {
        this.soldier = null;
    }

    public void NotifyAboutKingdomDefeated()
    {
        Debug.Log($"Tile {this.gameObject.name} was notified about defeat");
        this.kingdom = null;
        this.ResetColor();
    }
}