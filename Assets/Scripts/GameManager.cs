using System;
using Helpers.Extensions;
using TurnHandling;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        LevelGeneration,
        SoldierStartPositionSelection,
        Playing,
        EndScreen
    }

    public static GameManager Instance { get; private set; }

    public delegate void TurnStartEventDelegate(Kingdom kingdomNowPlaying);
    public event TurnStartEventDelegate TurnStartEvent;

    public bool isPaused = false;

    public AudioSource sound;

    [SerializeField] private GameAudio AudioScript;

    [SerializeField] private GameObject soldierInstance;

    [SerializeField] private TextAsset[] levels;

    [SerializeField, ReadOnlyInspector] private int currentLevel = 0;

    private int PlayerCount => GlobalManager.Instance.playerCount;

    [SerializeField, ReadOnlyInspector] private Kingdom[] playerKingdoms;

    private RingBuffer<Kingdom> kingdomRingBuffer;

    public event Action<Kingdom> WinnerChosenEvent;

    [SerializeField] private BoardManager boardManager;

    public event Action<GameState> GameStateChangedEvent;

    [SerializeField, ReadOnlyInspector] private GameState currentGameState = GameState.LevelGeneration;

    private GameState CurrentGameState
    {
        get => this.currentGameState;
        set
        {
            if (value == this.currentGameState)
            {
                return;
            }

            this.currentGameState = value;
            this.GameStateChangedEvent?.Invoke(value);
        }
    }

    private ITurnHandler turnHandler;

    public GameObject SoldierInstance => this.soldierInstance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        this.GenerateKingdoms();
        this.GenerateLevel();
        this.StartSoldierPositionSelection();

        this.kingdomRingBuffer.OnlyOneEntryRemainingEvent += this.HandleOnlyOneEntryRemainingEvent;
    }

    /// <summary>
    /// This Method is invoked when only one Element remains in a Ring Buffer.
    /// In this instance this RingBuffer handles all "alive" Kingdoms.
    /// In the instance where only one Kingdom remains we have a "Winner"
    /// </summary>
    /// <param name="winningKingdom"></param>
    private void HandleOnlyOneEntryRemainingEvent(Kingdom winningKingdom)
    {
        Debug.LogWarning("A Winner was chosen");
        Debug.Log(winningKingdom.Name);

        this.CurrentGameState = GameState.EndScreen;
        this.NotifyAboutTurnComplete();

        this.WinnerChosenEvent?.Invoke(winningKingdom);
    }

    private void StartSoldierPositionSelection()
    {
        this.CurrentGameState = GameState.SoldierStartPositionSelection;
        var currentKingdom = this.kingdomRingBuffer.Peek();
        this.turnHandler = new SoldierStartPositionSelectionTurnHandler(3,
            currentKingdom, this.boardManager);
        this.turnHandler.TurnFinishedEvent += this.NotifyAboutTurnComplete;
        this.TurnStartEvent?.Invoke(currentKingdom);
    }

    private void GenerateKingdoms()
    {
        this.playerKingdoms = new Kingdom[this.PlayerCount];

        var kingdomRoot = new GameObject("Kingdoms");


        for (int i = 0; i < this.PlayerCount; i++)
        {
            var kingdomGameObject = new GameObject($"Kingdom {i + 1}");
            var kingdom = kingdomGameObject.AddComponent<Kingdom>();
            kingdom.Color = GlobalManager.Instance.playerKingdoms[i].Color;
            kingdom.KingdomCastleSprite = GlobalManager.Instance.playerKingdoms[i].KingdomCastleSprite;
            kingdom.Name = GlobalManager.Instance.playerKingdoms[i].Name;
            this.playerKingdoms[i] = kingdom;    //playerkingdoms -> globalmanager?

            kingdomGameObject.transform.parent = kingdomRoot.transform;
        }

        this.kingdomRingBuffer = new RingBuffer<Kingdom>(this.playerKingdoms);

        foreach (var entry in this.playerKingdoms)
        {
            entry.KingdomDefeatedEvent += kingdom =>
            {
                this.kingdomRingBuffer.Remove(kingdom);
                this.boardManager.NotifyAboutKingdomConquered(kingdom);
            };
        }

        this.playersThatNeedToSpawnPositions = this.PlayerCount;
        kingdomRoot.transform.parent = this.transform;

    }

    private void GenerateLevel()
    {
        var levelTextAsset = this.levels[GlobalManager.Instance.level - 1];
        var levelDefinition = new LevelDefinition(levelTextAsset.text);
        levelDefinition.Apply(this.boardManager, this.playerKingdoms);
    }

    public void playAudio(String i)
    {
        switch (i)
        {
            case "fight":
                AudioScript.PlayFight();
                break;

            case "move":
                AudioScript.PlayMove();
                break;

            case "sit":
                AudioScript.PlaySit();
                break;
            case "house":
                AudioScript.PlayHouse();
                break;
        }
    }

    /// <summary>
    /// This Method notifies the current Turn Handler (assuming this is invoked during regular play) that the current
    /// player should finish the turn immediately by setting all remaining soldiers to "Spread Influence"
    /// </summary>
    public void EndTurn()
    {
        foreach (var s in this.kingdomRingBuffer.Peek().Soldiers)
        {
            if (!s.CanMoveThisTurn())
            {
                continue;
            }

            s.ApplyInfluence();
            if (this.turnHandler is RegularTurnHandler handler)
            {
                handler.SetSoldierMoved(s);
            }
        }
    }
    [SerializeField, ReadOnlyInspector]
    private int playersThatNeedToSpawnPositions;

    private void NotifyAboutTurnComplete()
    {
        if (this.turnHandler != null)
        {
            this.turnHandler.TurnFinishedEvent -= this.NotifyAboutTurnComplete;
        }

        Debug.Log("Turn Complete");
        this.boardManager.SyncColors();
        if (this.CurrentGameState == GameState.SoldierStartPositionSelection)
        {
            this.playersThatNeedToSpawnPositions--;
            if (this.playersThatNeedToSpawnPositions <= 0)
            {
                // All players have selected their Soldier Spawns
                this.CurrentGameState = GameState.Playing;
                Debug.Log("Switching to \"Playing\" State");
            }
            else
            {
                this.kingdomRingBuffer.MoveHeadToTail();
                this.StartSoldierPositionSelection();
            }
        }
        if (this.CurrentGameState == GameState.Playing)
        {
            this.kingdomRingBuffer.MoveHeadToTail();
            var head = this.kingdomRingBuffer.Peek();
            var regularTurnHandler = new RegularTurnHandler(head, this.boardManager);
            if (regularTurnHandler.MovableSoldierCount == 0)
            {
                // If in here: No Soldier could move. As a result, this kingdom has lost.
                var lostKingdom = this.kingdomRingBuffer.Pop();
                lostKingdom.NotifyAboutDefeat();
                this.boardManager.NotifyAboutKingdomConquered(lostKingdom);
                head = this.kingdomRingBuffer.Peek();
                regularTurnHandler = new RegularTurnHandler(head, this.boardManager);
            }

            this.turnHandler = regularTurnHandler;
            this.turnHandler.TurnFinishedEvent += this.NotifyAboutTurnComplete;
            this.TurnStartEvent?.Invoke(head);
        }
        else if (this.CurrentGameState == GameState.EndScreen)
        {
            this.turnHandler = null;
        }

    }

    public void NotifyAboutMouseDown(TileState tile, bool primary)
    {
        if (!this.isPaused)
        {
            this.turnHandler?.NotifyAboutMouseDown(tile, primary);
        }
    }

    public void NotifyAboutMouseUp(TileState tile, bool primary)
    {
        if (!this.isPaused)
        {
            this.turnHandler?.NotifyAboutMouseUp(tile, primary);
        }
    }
}

