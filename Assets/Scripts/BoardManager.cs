using System;
using System.Collections.Generic;
using System.Linq;
using Helpers.Clustering;
using UnityEngine;
using Random = System.Random;

public class BoardManager : MonoBehaviour
{
    [SerializeField] private List<Sprite> tileSprites;
    [SerializeField] internal GameObject tileInstance;
    [SerializeField] internal GameObject kingdomTileInstance;
    [SerializeField] internal GameObject houseTileInstance;
    [SerializeField] internal float xOffset;
    [SerializeField] internal float yOffset;

    private TileState[,] tiles;
    private List<TileState> kingdomTiles = new List<TileState>();
    private IReadOnlyList<TileState> allTiles;
    private readonly Random random = new Random();


    public (float minX, float maxX, float minY, float maxY) Bounds { get; private set; }

    public event Action NotifyBuildCompleteEvent;

    public void PushNewTiles(TileState[,] tileStates, List<TileState> kingdomTiles)
    {
        this.tiles = tileStates;
        this.kingdomTiles = kingdomTiles;
        var list = new List<TileState>();

        float minX = float.NaN, maxX  = float.NaN, minY  = float.NaN, maxY  = float.NaN;

        foreach (var entry in this.tiles)
        {
            if (entry != null)
            {
                // Add Position to bounds
                BoardManager.UpdateBounds(entry, ref minX, ref maxX, ref minY, ref maxY);
                // Add to list of all tiles
                list.Add(entry);
            }

        }
        this.allTiles = list;
        this.Bounds = (minX, maxX, minY, maxY);
        this.NotifyBuildCompleteEvent?.Invoke();
    }

    private static void UpdateBounds(Component entry, ref float minX, ref float maxX, ref float minY, ref float maxY)
    {
        var position = entry.gameObject.transform.position;
        if (position.x < minX || float.IsNaN(minX))
        {
            minX = position.x;
        }

        if (position.x > maxX || float.IsNaN(maxX))
        {
            maxX = position.x;
        }

        if (position.y < minY || float.IsNaN(minY))
        {
            minY = position.y;
        }

        if (position.y > maxY || float.IsNaN(maxY))
        {
            maxY = position.y;
        }
    }

    public void Reset()
    {
        this.tiles = new TileState[0, 0];
        this.allTiles = new List<TileState>();
        foreach (Transform entry in transform)
        {
            Destroy(entry.gameObject);
        }
    }

    //public void

    public List<TileState> GetTilesSurrounding((int x, int y) position)
    {
        var (x, y) = position;

        int xCount = tiles.GetLength(0);
        int yCount = tiles.GetLength(1);

        (int x, int y)[] surroundingTileIndices;

        if (y % 2 != 0)
        {
            surroundingTileIndices = new (int x, int y)[]
            {
                (x, y - 1), (x + 1, y - 1), (x + 1, y), (x + 1, y + 1), (x, y + 1), (x - 1, y)
            };
        }
        else
        {
            surroundingTileIndices = new (int x, int y)[]
            {
                (x - 1, y - 1), (x , y - 1), (x + 1, y), (x , y + 1), (x -1, y + 1), (x - 1, y)
            };
        }



        var surroundingTilesInBounds = from surroundingTileIndex in surroundingTileIndices
            where surroundingTileIndex.x >= 0 && surroundingTileIndex.x < xCount && surroundingTileIndex.y >= 0 &&
                  surroundingTileIndex.y < yCount
            select surroundingTileIndex;
        var surroundingTiles = from tile in surroundingTilesInBounds
            where this.tiles[tile.x, tile.y] != null
            select this.tiles[tile.x, tile.y];

        return surroundingTiles.ToList();
    }

    public TileState GetKingdomTile(Kingdom kingdom)
    {
        return this.kingdomTiles.Find(e => e.Kingdom == kingdom);
    }

    public void SyncColors()
    {
        foreach (var entry in this.tiles)
        {
            if ((object) entry == null)
            {
                continue;
            }
            entry.ResetColor();
            if (entry.Soldier != null)
            {
                entry.Soldier.UpdateColor();
            }
        }
    }

    public void RunCutOffCalculations()
    {
        try
        {
            var result = new CutOffTilesResult(this.allTiles);
            result.NotifyAllTilesStatesAboutNewCutOffState();
        }
        catch {
            Debug.LogError("Cluster Bruh");
        }
    }

    private void UnsetAllTilesFromKingdom(Kingdom lostKingdom)
    {
        foreach (var entry in this.allTiles)
        {
            if (entry.Kingdom == lostKingdom)
            {
                entry.NotifyAboutKingdomDefeated();
            }
        }
    }

    public void NotifyAboutKingdomConquered(Kingdom kingdom)
    {
        this.UnsetAllTilesFromKingdom(kingdom);
    }

    public Sprite GetRandomTileTexture() => this.tileSprites[this.random.Next(this.tileSprites.Count)];

}