using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiScript : MonoBehaviour
{
    [SerializeField] private GameObject UI;
    [SerializeField, ReadOnlyInspector] private GameObject UiPlayerTurn;
    [SerializeField, ReadOnlyInspector] private GameObject TxtPlayerTurn;
    [SerializeField, ReadOnlyInspector] private GameObject BtnPlayerTurn;

    [SerializeField, ReadOnlyInspector] private GameObject WindowPause;
    [SerializeField, ReadOnlyInspector] private GameObject WindowWin;

    private bool btnHud = false;

    //private bool isGamePaused = false;

    void Start()
    {
        this.UI = this.gameObject.transform.GetChild(0).gameObject;
        this.UiPlayerTurn = UI.transform.GetChild(0).gameObject;
        this.TxtPlayerTurn = UiPlayerTurn.gameObject.transform.GetChild(0).gameObject;
        this.BtnPlayerTurn = UI.transform.GetChild(1).gameObject;
        GameManager.Instance.GameStateChangedEvent += this.EnableButton;

        WindowPause = this.gameObject.transform.GetChild(1).GetChild(0).gameObject;
        WindowWin = this.gameObject.transform.GetChild(1).GetChild(1).gameObject;
        GameManager.Instance.TurnStartEvent += this.HandleTurnStart;
        GameManager.Instance.WinnerChosenEvent += this.OpenWin;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.PauseMenu();
        }
    }

    public void PauseMenu()
    {
        var manager = GameManager.Instance;
        manager.isPaused = !manager.isPaused;
        WindowPause.SetActive(manager.isPaused);
        if (this.btnHud)
        {
            this.BtnPlayerTurn.SetActive(!manager.isPaused);
        }
    }

    public void OpenWin(Kingdom winningKingdom)
    {
        this.WindowWin.transform.GetChild(0).gameObject.GetComponent<Text>().text = winningKingdom.Name + " WON";
        this.WindowWin.SetActive(true);
        this.UI.SetActive(false);
    }

    public void BackToMenu()
    {
        GlobalManager.LoadMenu();
    }

    public void HandleTurnStart(Kingdom kingdomNowPlaying)
    {
        this.UpdateHud(kingdomNowPlaying);
    }

    public void UpdateHud(Kingdom kingdomNowPlaying)
    {
        UiPlayerTurn.GetComponent<Image>().color = kingdomNowPlaying.Color;
        TxtPlayerTurn.GetComponent<Text>().text = kingdomNowPlaying.Name;
    }

    public void EndTurnButton()   //when next players turn
    {
        GameManager.Instance.EndTurn();
    }

    private void EnableButton(GameManager.GameState state)
    {
        if(state == GameManager.GameState.Playing)
        {
            this.BtnPlayerTurn.SetActive(true);
            this.btnHud = true;
        }
    }
}