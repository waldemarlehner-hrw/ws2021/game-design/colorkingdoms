using System;
using System.Collections.Generic;
using UnityEngine;

public class Kingdom : MonoBehaviour
{
    [SerializeField, ReadOnlyInspector] private Sprite kingdomCastleSprite;

    [SerializeField] private Color kingdomColor;
    
    [SerializeField] private string kingdomName;

    [SerializeField] [ReadOnlyInspector] private List<Soldier> soldiers = new List<Soldier>();

    [SerializeField, ReadOnlyInspector] private bool wasDefeated = false;

    private event Action KingdomDefeatedEventInternal;
    public event Action<Kingdom> KingdomDefeatedEvent;
    public event Action NewTurnStartedEvent;
    public event Action TurnFinishedEvent;

    public Color Color
    {
        get => this.kingdomColor;
        set => this.kingdomColor = value;
    }

    public string Name
    {
        get => this.kingdomName;
        set => this.kingdomName = value;
    }

    public Sprite KingdomCastleSprite
    {
        get => this.kingdomCastleSprite;
        set => this.kingdomCastleSprite = value;
    }

    public void SpawnSoldierOnTile(TileState tile)
    {
        var soldier = Instantiate(GameManager.Instance.SoldierInstance, this.transform, true);
        var soldierScript = soldier.GetComponent<Soldier>();
        soldierScript.Init(this, tile);

        soldierScript.OnDeadEvent += () => this.HandleSoldierDeathEvent(soldierScript);
        this.soldiers.Add(soldierScript);

        tile.MoveSoldierHere(soldierScript);
    }

    private void HandleSoldierDeathEvent(Soldier soldier)
    {
        Debug.Log("Killing Soldier");
        this.soldiers.Remove(soldier);


        if (this.soldiers.Count == 0)
        {
            this.NotifyAboutDefeat();
        }
    }



    public void NotifyAboutNewTurn()
    {
        this.NewTurnStartedEvent?.Invoke();
    }

    public void NotifyAboutTurnFinished()
    {
        this.TurnFinishedEvent?.Invoke();
    }

    public IReadOnlyList<Soldier> Soldiers => this.soldiers;

    public void NotifyAboutDefeat()
    {
        if (this.wasDefeated)
        {
            return;
        }

        this.wasDefeated = true;

        this.KingdomDefeatedEventInternal?.Invoke();

        this.KingdomDefeatedEvent?.Invoke(this);
    }

    /// <summary>
    /// This Method should only ever be invoked by the Soldiers to subscribe to defeats
    /// </summary>
    /// <param name="handleKingdomDefeat">Action that should be invoked when the defeat takes place. This should be a Method inside the Soldier</param>
    public void SubscribeDefeatInternal(Action handleKingdomDefeat)
    {
        this.KingdomDefeatedEventInternal += handleKingdomDefeat;
    }

    public void UnsubscribeDefeatInternal(Action handleKingdomDefeat)
    {
        this.KingdomDefeatedEventInternal -= handleKingdomDefeat;
    }
}
