using System;
using UnityEngine;

public class KingdomTile : MonoBehaviour
{
    [SerializeField] private SpriteRenderer accentSpriteRenderer;

    [SerializeField, ReadOnlyInspector] private Kingdom kingdom;

    private bool init;

    public void Init(Kingdom kingdom)
    {
        if (this.init)
        {
            throw new Exception("Object already initialized");
        }

        this.kingdom = kingdom ? kingdom : throw new NullReferenceException("Passed Kingdom cannot be null");
        this.accentSpriteRenderer.sprite = kingdom.KingdomCastleSprite;
        this.init = true;
        this.gameObject.GetComponent<TileState>().KingdomChangedFromToEvent += this.HandleKingdomTileChangeKingdom;
    }


    private void HandleKingdomTileChangeKingdom(Kingdom old, Kingdom @new)
    {
        if (old != this.kingdom)
        {
            throw new Exception($"Inconsistent Kingdoms. Expected Old to be ${this.kingdom}, but found ${old}");
        }

        this.gameObject.GetComponent<TileState>().KingdomChangedFromToEvent -= this.HandleKingdomTileChangeKingdom;

        this.kingdom.NotifyAboutDefeat();
        Destroy(this);
    }
}
